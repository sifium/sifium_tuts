var app=angular.module("myapp",[]);
app.service('mathservice',function(){
	this.add=function(x,y) {
	   	return x+y;
	};
	this.sub=function(x,y) {
	   	return x-y;
	};
	this.mul=function(x,y) {
	   	return x*y;
	};
	this.div=function(x,y) {
	   	return x/y;
	};
	this.fact=function(x) {

		var f=1;

		for(var i=x;i>0;i--)
		{
          f=f*i;

		}
	   	return f;
	};

});
app.controller('myctrl',function($scope,mathservice){
	$scope.doadd=function(){
       $scope.answer=mathservice.add($scope.test1,$scope.test2);
   };
   $scope.dosub=function(){
       $scope.answer=mathservice.sub($scope.test1,$scope.test2);
   };
   $scope.domul=function(){
       $scope.answer=mathservice.mul($scope.test1,$scope.test2);
   };
   $scope.dodiv=function(){
       $scope.answer=mathservice.div($scope.test1,$scope.test2);
   };
   $scope.dofact=function(){
       $scope.answer=mathservice.fact($scope.test1);
   };

});
var myapp=angular.module("myapp",[]);
myapp.directive('personinfo',function() {
	var directive={};
	directive.restrict='E';//means directive is of Element type
	directive.template="PersonName:{{person.name}},PhoneNo:{{person.phno}}";
	directive.scope={
		person:"=ename"
	};

	directive.compile=function(element,attributes){
		//element.css("border","3 px solid #gg00gg");
        var linkFunction =function($scope,element,attributes){
        	//element.html("Person Name:"+$scope.person.name+",+Phone No:"+$scope.person.phno+"");
        	 element.css("background-color", "#ff00ff");
        }
        return linkFunction;
	}
	return directive;
});
myapp.controller('personctrl',function($scope){
	$scope.Sourav={};
	$scope.Sourav.name="Sourav";
	$scope.Sourav.phno="123456";
	$scope.Chandra={};
	$scope.Chandra.name="Chandra";
	$scope.Chandra.phno="9779655245";
} );



var mongoose = require('mongoose');

module.exports = mongoose.model('User',{
	email: String,
	oauthID:Number,
	firstName: String,
	lastName: String,
	token:String
});
var config = require('../oauth.js');
var LocalStrategy   = require('passport-local').Strategy;
var FacebookStrategy   = require('passport-facebook').Strategy;
var User = require('../models/user');


module.exports = function(passport){

     // used to serialize the user for the session
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    // used to deserialize the user
    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

	passport.use(new FacebookStrategy({
            clientID: config.facebook.clientID,
             clientSecret: config.facebook.clientSecret,
             callbackURL: config.facebook.callbackURL
        },

    function(accessToken, refreshToken, profile, done) { 
        process.nextTick(function () {
            User.findOne({'oauthID':profile.id},function(err,user){
                if(err) return done(err);

                if(user){
                    return done(null,user);
                } else{
console.log(profile);

                    var newUser=new User();
                    // set the user's local credentials
                    newUser.oauthID = profile.id;
                    newUser.token=accessToken;
                    newUser.firstName = profile.name.givenName;
                    newUser.lastName = profile.name.familyName;
                    newUser.email=profile.emails[0].value;

                    // save the user
                    newUser.save(function(err) {
                        if (err){
                            console.log('Error in Saving user: '+err);  
                            throw err;  
                        }
                        console.log('User Registration succesful');    
                        return done(null, newUser);
                    });
                }
            });
         });
        
    }));

    // test authentication
    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) return next(); 
        res.redirect('/');
    }
    
}
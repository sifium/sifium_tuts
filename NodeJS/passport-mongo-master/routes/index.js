function isLoggedIn (req, res, next) {
	if (req.isAuthenticated()) return next(); 
	res.redirect('/');
}


module.exports = function(app,passport){

	/* GET login page. */
	app.get('/', function(req, res) {
    	// Display the Login page with any flash message, if any
		res.render('index', { message: req.flash('message') });
	});
	
	/* GET Home Page */
	app.get('/home', isLoggedIn, function(req, res){
		
		res.render('home', { user: req.user });

	});

	/* Handle Logout */
	app.get('/signout', function(req, res) {
		req.logout();
		res.redirect('/');
	});


	//FACEBOOK
	app.get('/auth/facebook', passport.authenticate('facebook',{scope:['email']}));

	app.get('/auth/facebook/callback', passport.authenticate('facebook', {successRedirect:'/home', failureRedirect: '/' }));
}





